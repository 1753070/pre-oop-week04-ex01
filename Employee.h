#ifndef CLIONPROJECT_EMPLOYEE_H
#define CLIONPROJECT_EMPLOYEE_H

#include <iostream>
#include <fstream>
#include <string>

#include "Date.h"
using namespace std;

class Employee {
protected:
    string _id, _name;
    Date *_dob;
    string _address;

    bool CheckValidContentInString();

public:
    Employee();

    virtual ~Employee() = 0;

    virtual double Salary() = 0;

    virtual void Input(istream &input) = 0;
    virtual void Output(ostream &output) = 0;

    virtual string Type() = 0;

    virtual Employee *Clone() = 0;
};


#endif //CLIONPROJECT_EMPLOYEE_H
