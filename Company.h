#ifndef CLIONPROJECT_COMPANY_H
#define CLIONPROJECT_COMPANY_H

#include "DailyEmployee.h"
#include "ProductionEmployee.h"

#include <vector>

class Company final {
private:
    Employee **_listEmployees;
    int _numberOfEmployees;

public:
    Company();

    void Input();
    void Output();

    Employee *BiggestSalary();

    int NumberOfType(const string &type);

    double SumOfSalary();

    double AverageOfSalary();

    vector<Employee *> LowSalaryEmployees();

    ~Company();
};


#endif //CLIONPROJECT_COMPANY_H
