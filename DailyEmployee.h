#ifndef CLIONPROJECT_DAILYEMPLOYEE_H
#define CLIONPROJECT_DAILYEMPLOYEE_H

#include "Employee.h"

class DailyEmployee : virtual public Employee {
private:
    int _dayWorked;

public:
    DailyEmployee();
    DailyEmployee(const DailyEmployee &employee);
    DailyEmployee(const string &id, const string &name, const int &dayWorked = 0);
    DailyEmployee(const string &id, const string &name, const string &address, const int &dayWorked = 0);
    DailyEmployee(const string &id, const string &name, const string &address, const Date &dob,
                    const int &dayWorked = 0);
    DailyEmployee(const string &id, const string &name, const string &address,
                    const int &dayOfBirth, const int &monthOfBirth, const int &yearOfBirth,
                    const int &dayWorked = 0);

    double Salary() override;

    string Type() override;

    void Input(istream &input) override;
    void Output(ostream &output) override;

    Employee *Clone() override;
};


#endif //CLIONPROJECT_DAILYEMPLOYEE_H
