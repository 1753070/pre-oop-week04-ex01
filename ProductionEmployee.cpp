#include "ProductionEmployee.h"

ProductionEmployee::ProductionEmployee() {
    _id = _name = _address = "";
    _dob = nullptr;
    _numberOfProducts = 0;
}

ProductionEmployee::ProductionEmployee(const ProductionEmployee &employee) {
    _id = employee._id;
    _name = employee._name;
    _address = employee._address;
    _dob = new Date(*_dob);
    _numberOfProducts = employee._numberOfProducts;
}

ProductionEmployee::ProductionEmployee(const string &id, const string &name,
                                       const int &numberOfProducts) {
    _id = id;
    _name = name;
    _numberOfProducts = (numberOfProducts >= 0) ? numberOfProducts : 0;
}

ProductionEmployee::ProductionEmployee(const string &id, const string &name, const string &address,
                                       const int &numberOfProducts) {
    _id = id;
    _name = name;
    _address = address;
    _numberOfProducts = (numberOfProducts >= 0) ? numberOfProducts : 0;
}

ProductionEmployee::ProductionEmployee(const string &id, const string &name, const string &address, const Date &dob,
                                       const int &numberOfProducts) {
    _id = id;
    _name = name;
    _address = address;
    _dob = new Date(dob);
    _numberOfProducts = (numberOfProducts >= 0) ? numberOfProducts : 0;
}

ProductionEmployee::ProductionEmployee(const string &id, const string &name, const string &address,
                                       const int &dayOfBirth, const int &monthOfBirth, const int &yearOfBirth,
                                       const int &numberOfProducts) {
    _id = id;
    _name = name;
    _address = address;
    _dob = new Date(dayOfBirth, monthOfBirth, yearOfBirth);
    _numberOfProducts = (numberOfProducts >= 0) ? numberOfProducts : 0;
}

void ProductionEmployee::Input(istream &input) {
    _dob = new Date;
    while (true) {
        cout << "Enter employee's ID: ";
        getline(input, _id);
        cout << "Enter employee's name: ";
        getline(input, _name);
        cout << "Enter employee's address: ";
        getline(input, _address);
        cout << "Enter employee's date of birth: ";
        input >> *_dob;
        cout << "Enter employees's products amount: ";
        input >> _numberOfProducts;

        string cache;
        getline(input, cache);

        if (!CheckValidContentInString()) {
            cout << "One of these field is invalid: ID, name, address. Please try again." << endl;
            continue;
        }
        if (_numberOfProducts < 0) {
            cout << "Number of products can not smaller than 0. Please try again." << endl;
            continue;
        }
        break;
    }
}

void ProductionEmployee::Output(ostream &output) {
    output << "Employee's ID: " << _id << endl;
    output << "Employee's name: " << _name << endl;
    output << "Employee's address: " << _address << endl;
    output << "Employee's date of birth: " << *_dob;
    output << "Number of products: " << _numberOfProducts << endl;
}



double ProductionEmployee::Salary() {
    return _numberOfProducts * 20000;
}

string ProductionEmployee::Type() {
    return "Production";
}

Employee *ProductionEmployee::Clone() {
    return new ProductionEmployee(*this);
}
