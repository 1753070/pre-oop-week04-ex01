#include "Company.h"

Company::Company() {
    _listEmployees = nullptr;
    _numberOfEmployees = 0;
}

Company::~Company() {
    for (int i = 0; i < _numberOfEmployees; ++i)
        delete _listEmployees[i];
    delete _listEmployees;
}

void Company::Input() {
    cout << "Enter number of employees: ";
    cin >> _numberOfEmployees;
    string cache;
    getline(cin, cache);

    if (_numberOfEmployees > 0)
        _listEmployees = new Employee*[_numberOfEmployees];

    string type;
    for (int i = 0; i < _numberOfEmployees; ++i) {
        while (true) {
            cout << "Which type of employee you want to add (product or daily): ";
            getline(cin, type);
            if (type != "product" && type != "daily")
                continue;
            if (type == "product")
                _listEmployees[i] = new ProductionEmployee();
            else
                _listEmployees[i] = new DailyEmployee();
            _listEmployees[i]->Input(cin);
            break;
        }
    }
}

void Company::Output() {
    cout << "Number of employees: " << _numberOfEmployees;
    for (int i = 0; i < _numberOfEmployees; ++i)
        _listEmployees[i]->Output(cout);
}

Employee *Company::BiggestSalary() {
    double biggest = _listEmployees[0]->Salary();
    int markPosition = 0;

    for (int i = 1; i < _numberOfEmployees; ++i) {
        if (_listEmployees[i]->Salary() > biggest) {
            markPosition = i;
            biggest = _listEmployees[i]->Salary();
        }
    }

    return _listEmployees[markPosition]->Clone();
}

int Company::NumberOfType(const string &type) {
    int numberOfType = 0;
    for (int i = 0; i < _numberOfEmployees; ++i)
        if (_listEmployees[i]->Type() == type)
            ++numberOfType;
    return numberOfType;
}

double Company::SumOfSalary() {
    double sum = 0;
    for (int i = 0; i < _numberOfEmployees; ++i)
        sum += _listEmployees[i]->Salary();
    return sum;
}

double Company::AverageOfSalary() {
    return SumOfSalary() / _numberOfEmployees;
}

vector<Employee *> Company::LowSalaryEmployees() {
    vector<Employee *> listLowSalary;
    for (int i = 0; i < _numberOfEmployees; ++i)
        if (_listEmployees[i]->Salary() < 3000000) {
        Employee *employee = _listEmployees[i]->Clone();
        listLowSalary.push_back(employee);
    }
}
