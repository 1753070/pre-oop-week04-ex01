#include "Employee.h"

Employee::Employee() {
    _id = _name = _address = "";
    _dob = nullptr;
}

bool Employee::CheckValidContentInString() {
    bool checkId = false;
    for (auto& i : _id)
        if ((i >= 'A' && i <= 'Z') || (i >= 'a' && i <= 'z') || (i >= '0' && i <= '9'))
            checkId = true;

    bool checkName = false;
    for (auto& i : _name)
        if ((i >= 'A' && i <= 'Z') || (i >= 'a' && i <= 'z') || (i >= '0' && i <= '9'))
            checkName = true;

    bool checkAddress = false;
    for (auto& i : _address)
        if ((i >= 'A' && i <= 'Z') || (i >= 'a' && i <= 'z') || (i >= '0' && i <= '9'))
            checkAddress = true;

    return checkId && checkName && checkAddress;

}

Employee::~Employee() {
    _id.clear();
    _name.clear();
    _address.clear();
    delete _dob;
}