#ifndef CLIONPROJECT_DATE_H
#define CLIONPROJECT_DATE_H

#include <iostream>
using namespace std;

class Date {
private:
    int _day, _month, _year;

    bool ValidDate();

public:
    Date();
    Date(const int &day, const int &month, const int &year);
    Date(const Date &date);

    friend istream &operator>>(istream &input, Date &date);
    friend ostream &operator<<(ostream &output, const Date &date);
};


#endif //CLIONPROJECT_DATE_H
