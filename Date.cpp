#include "Date.h"

bool Date::ValidDate() {
    if (this->_day <= 0 || this->_month <= 0 || this->_year <= 0)
        return false;

    if (this->_month > 12)
        return false;

    switch (this->_month) {
        case 1: case 3: case 5: case 7: case 8: case 10: case 12:
            if (this->_day > 31)
                return false;
            break;
        case 2:
            if (this->_year % 100 == 0) {
                if (this->_year % 400 == 0 && this->_day > 29)
                    return false;
                else if (this->_year % 400 != 0 && this->_day > 28)
                    return false;
            } else if (this->_year % 4 == 0 && this->_day > 29) {
                return false;
            } else if (this->_year % 4 != 0 && this->_day > 28) {
                return false;
            }
            break;
        default:
            if (this->_day > 30)
                return false;
            break;
    }

    return true;
}

Date::Date() {
    this->_day = this->_month = this->_year = 1;
}

Date::Date(const int &day, const int &month, const int &year) {
    this->_day = day;
    this->_month = month;
    this->_year = year;

    if (!this->ValidDate()) {
        cout << "Invalid Date. The program will now terminated." << endl;
        exit(EXIT_FAILURE);
    }
}

Date::Date(const Date &date) {
    this->_day = date._day;
    this->_month = date._month;
    this->_year = date._year;
}


istream &operator>>(istream &input, Date &date) {
    while (true) {
        cout << "Enter day, month, year of the date (Ex: 10 01 1970): " << endl;
        input >> date._day >> date._month >> date._year;
        if (!date.ValidDate()) {
            cout << "Invalid Date. Try again." << endl;
            continue;
        }
        break;
    }
    return input;
}

ostream &operator<<(ostream &output, const Date &date) {
    output << date._day << "/" << date._month << "/" << date._year << endl;
    return output;
}
