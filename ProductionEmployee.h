#ifndef CLIONPROJECT_PRODUCTIONEMPLOYEE_H
#define CLIONPROJECT_PRODUCTIONEMPLOYEE_H

#include "Employee.h"

class ProductionEmployee final : virtual public Employee {
private:
    int _numberOfProducts;
public:
    ProductionEmployee();
    ProductionEmployee(const ProductionEmployee &employee);
    ProductionEmployee(const string &id, const string &name, const int &numberOfProducts = 0);
    ProductionEmployee(const string &id, const string &name, const string &address, const int &numberOfProducts = 0);
    ProductionEmployee(const string &id, const string &name, const string &address, const Date &dob,
                        const int &numberOfProducts = 0);
    ProductionEmployee(const string &id, const string &name, const string &address,
                        const int &dayOfBirth, const int &monthOfBirth, const int &yearOfBirth,
                        const int &numberOfProducts = 0);

    void Input(istream &input) override;
    void Output(ostream &output) override;

    double Salary() override;

    string Type() override;

    Employee *Clone() override;
};


#endif //CLIONPROJECT_PRODUCTIONEMPLOYEE_H
