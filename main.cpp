#include "Company.h"

int main() {
    Company *company = new Company();
    company->Input();
    Employee *highest = company->BiggestSalary();
    highest->Output(cout);
    delete highest;
    delete company;
    return EXIT_SUCCESS;
}