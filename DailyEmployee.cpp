#include "DailyEmployee.h"

DailyEmployee::DailyEmployee() {
    _id = _name = _address = "";
    _dayWorked = 0;
    _dob = nullptr;
}

DailyEmployee::DailyEmployee(const string &id, const string &name, const int &dayWorked) {
    _id = id;
    _name = name;
    _dayWorked = (dayWorked >= 0) ? dayWorked : 0;
    _dob = nullptr;
}

DailyEmployee::DailyEmployee(const string &id, const string &name, const string &address, const int &dayWorked) {
    _id = id;
    _name = name;
    _address = address;
    _dayWorked = (dayWorked >= 0) ? dayWorked : 0;
    _dob = nullptr;
}

DailyEmployee::DailyEmployee(const DailyEmployee &employee) {
    _id = employee._id;
    _name = employee._name;
    _address = employee._address;
    _dayWorked = employee._dayWorked;
    _dob = new Date(*employee._dob);
}

DailyEmployee::DailyEmployee(const string &id, const string &name, const string &address, const Date &dob,
                             const int &dayWorked) {
    _id = id;
    _name = name;
    _address = address;
    _dob = new Date(dob);
    _dayWorked = (dayWorked >= 0) ? dayWorked : 0;
}

DailyEmployee::DailyEmployee(const string &id, const string &name, const string &address, const int &dayOfBirth,
                             const int &monthOfBirth, const int &yearOfBirth, const int &dayWorked) {
    _id = id;
    _name = name;
    _address = address;
    _dob = new Date(dayOfBirth, monthOfBirth, yearOfBirth);
    _dayWorked = (dayWorked >= 0) ? dayWorked : 0;
}

double DailyEmployee::Salary() {
    return _dayWorked * 300000;
}

string DailyEmployee::Type() {
    return "Daily";
}

void DailyEmployee::Input(istream &input) {
    _dob = new Date;
    while (true) {
        cout << "Enter employee's ID: ";
        getline(input, _id);
        cout << "Enter employee's name: ";
        getline(input, _name);
        cout << "Enter employee's address: ";
        getline(input, _address);
        cout << "Enter employee's date of birth: ";
        input >> *_dob;
        cout << "Enter employees's days had worked: ";
        input >> _dayWorked;

        string cache;
        getline(input, cache);

        if (!CheckValidContentInString()) {
            cout << "One of these field is invalid: ID, name, address. Please try again." << endl;
            continue;
        }
        if (_dayWorked < 0) {
            cout << "Days had worked can not smaller than 0. Please try again." << endl;
            continue;
        }
        break;
    }
}

void DailyEmployee::Output(ostream &output) {
    output << "Employee's ID: " << _id << endl;
    output << "Employee's name: " << _name << endl;
    output << "Employee's address: " << _address << endl;
    output << "Employee's date of birth: " << *_dob;
    output << "Days had worked: " << _dayWorked << endl;
}

Employee *DailyEmployee::Clone() {
    return new DailyEmployee(*this);
}
